ALTER TABLE payment_schedule ADD COLUMN isduedatepaymentschedule BOOLEAN DEFAULT FALSE;
ALTER TABLE aswa_histo.payment_schedule_histo ADD COLUMN isduedatepaymentschedule BOOLEAN DEFAULT FALSE;